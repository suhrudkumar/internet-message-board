## Internet message board

Internet message board is a simple rest application which allows users to post messages, list posted messages and delete messages.
It is a flask web app which is deployed using docker on Amazon Web Services

---

## Deploying the application

Run the following steps to deploy the application.

1. Clone the repository - ```` git clone https://suhrudkumar@bitbucket.org/suhrudkumar/internet-message-board.git ````
2. Change to terraform directory - ```` cd internet-message-board/terraform/ ````
3. Run ```` terraform init ```` to initialize terraform in current directory.
4. Run ```` terraform plan ```` to list of resources that are going to be provisioned.
5. Run ```` terraform apply ```` to deploy the resources.

Note - You need to have AWS API keys for terraform to provision resources. For more details, refer - https://www.terraform.io/docs/providers/aws/#authentication

EC2 instance and other resources will be provisioned in ap-south-1 region (Mumbai) by default.
Also, SSH access is not enabled by default. If you need SSH access to EC2 instance, uncomment the following line (line 89) in deploy-ec2.tf file and give an existing key name as value.
````key_name = "aws-test"````

---

## Requirements

1. Python - 3.6
2. pip packages
	* flask - 1.0.2
	* uWSGI - 2.0.17
3. docker

---

## Running the application locally

You can also run the application in your local machine. The application can run in three modes, development, testing and production (default).

First change to directory with Dockerfile and build the docker image using command - ```` docker build -t api_server . ```` 

1. You can run in development mode to enable features like debugger and code reloading with the following commands.
	
	```` docker run -d -e ENV=DEV -p 5000:5000 -v ${PWD}/app:/app api_server ````
	
	Here, we are binding our local directory as volume so that our code changes will be automatically picked up by the already running container.


2. Unit tests can be run by using following command.
	
	````` docker run -e ENV=UNIT  api_server `````


3. Finally, you can run the production uWSGI server using the following command
	
	```` docker run -d -p 9090:9090 -p 9191:9191 api_server ````
	
	Here web traffic will be served over port 9090 and stats will be available over port 9191


4. You can also run the production server using docker-compose. Run the following command in the directory with docker-compose.yml file
	
	```` docker-compose up -d ````
	
---	
	
## URI

Base URI - http://[hostname]:[port]/api/v1/messages/

| HTTP Method | URI                                             | Action                    |
|-------------|-------------------------------------------------|---------------------------|
| GET         | http://[hostname]/api/v1/messages/              | Retrieve list of messages |
| GET         | http://[hostname]/api/v1/messages/[message_id] | Retrieve a message        |
| POST        | http://[hostname]/api/v1/messages/              | Post a new message        |
| DELETE      | http://[hostname]/api/v1/messages/[message_id] | Delete a message          |


---	
	
## Usage


Note - All user requests must be in json format.


Get list of all messages posted



```
[root@ip-10-0-1-139 ~]# curl -i -X GET localhost:9090/api/v1/messages/
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 107
	 
{"messages":[{"body":"message one","id":1},{"body":"message two","id":2},{"body":"message three","id":3}]}
```


Get a single message by id


```
[root@ip-10-0-1-139 ~]# curl -i -X GET localhost:9090/api/v1/messages/2
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 63

{"message":{"body":"message two","id":2,"isPalindrome":false}}
```



Post a message



```
[root@ip-10-0-1-139 ~]# curl -i -H "Content-Type: application/json" -X POST -d '{"body":"step on no pets"}' http://localhost:9090/api/v1/messages/
HTTP/1.1 201 CREATED
Content-Type: application/json
Content-Length: 66

{"message":{"body":"step on no pets","id":4,"isPalindrome":true}}
```



Delete a message with id



~~~~
[root@ip-10-0-1-139 ~]# curl -i -X DELETE localhost:9090/api/v1/messages/4
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 30

{"Success":"message deleted"}
~~~~


---
