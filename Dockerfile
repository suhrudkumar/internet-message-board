FROM python:3.6

RUN groupadd -r uwsgi && useradd -r -g uwsgi uwsgi

COPY ./app /app

WORKDIR /app

RUN pip install -r requirements.txt

COPY cmd.sh /

EXPOSE 9090 9191

USER uwsgi

CMD ["/cmd.sh"]
