provider "aws" {
  region = "ap-south-1"
}


resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  enable_dns_hostnames = "true"

  tags {
    name = "main"
    created_by = "terraform"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    name = "main"
    created_by = "terraform"
  }
}


resource "aws_subnet" "main" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = "true"

  tags {
    name = "main"
    created_by = "terraform"
  }
}

resource "aws_route_table" "main" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main.id}"
  }

  tags {
    name = "main"
    created_by = "terraform"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = "${aws_subnet.main.id}"
  route_table_id = "${aws_route_table.main.id}"
}


resource "aws_security_group" "allow_all" {
  name        = "allow_all"
  description = "Allow all inbound traffic"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    name = "sg allow all"
    created_by = "terraform"
  }

}
 


# ec2 instance to deploy python flask rest app
resource "aws_instance" "message_board_server" {
   ami  = "ami-5a8da735"
   instance_type = "t2.micro"
   # key_name = "aws-test"
   subnet_id = "${aws_subnet.main.id}"
   vpc_security_group_ids = ["${aws_security_group.allow_all.id}"]
   user_data = "${file("bootstrap.sh")}"

  tags {
    name = "message_board_server"
    created_by = "terraform"
  }
}

output "public_ip" {
  value = "${aws_instance.message_board_server.public_ip}"
}

output "public_dns" {
  value = "${aws_instance.message_board_server.public_dns}"
}

