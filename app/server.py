from flask import Flask, jsonify, make_response, abort, request

app = Flask(__name__)

messages = [{'id': 1, 'body': 'message one'},{'id': 2, 'body': 'message two'},{'id': 3, 'body': 'message three'}]

@app.route('/api/v1/messages/', methods=['GET'])
def get_all_messages():

    return jsonify({'messages': messages})


@app.route('/api/v1/messages/<int:message_id>', methods=['GET'])
def get_message(message_id):
    
    for message in messages:
        if (message['id'] == message_id):
            # checks if string and its reverse are equal and sets the isPalindrome value accordingly
            message['isPalindrome'] = (message['body'][::-1] == message['body'])
            return jsonify({'message' : message})
    
    abort(404) 


@app.route('/api/v1/messages/', methods=['POST'])
def add_message():

    # make sure that input data is in correct format
    if not request.json or not 'body' in request.json or 'id' in request.json:
        abort(400)

    message = {}
    message['id'] = messages[-1]['id'] + 1 # to make sure that id's are unique
    message['body'] = request.json['body']

    messages.append(message)

    return get_message(message['id']), 201


@app.route('/api/v1/messages/<int:message_id>', methods=['DELETE'])
def delete_message(message_id):

    message_to_be_deleted = ""

    for message in messages:
        if message['id'] == message_id:
            message_to_be_deleted = message

    if message_to_be_deleted != "":
        messages.remove(message_to_be_deleted)
        return jsonify({'Success': 'message deleted'})
    else:
        abort(404)


@app.errorhandler(404)
def not_found(error):

    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(400)
def malformed_data(error):

    return make_response(jsonify({'error': 'Malformed data'}), 400)


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
