import unittest
import server

class TestCase(unittest.TestCase):

    def setUp(self):
        server.app.config["TESTING"] = True
        self.app = server.app.test_client()

    def test_get_all_messages(self):
        response = self.app.get("/api/v1/messages/")
        data = response.get_json()
        assert response.status_code == 200
        assert len(data['messages']) == 2
        assert 'message one' in data['messages'][0]['body']
        assert data['messages'][0]['id'] is not None


    def test_get_single_message(self):
        response = self.app.get("/api/v1/messages/2")
        data = response.get_json()
        assert response.status_code == 200
        assert 'message two' in data['message']['body']
        assert data['message']['id'] is not None
        assert data['message']['isPalindrome'] is False


    def test_post_message(self):
        response = self.app.post("/api/v1/messages/", json={'body':'radar'})
        data = response.get_json()
        assert response.status_code == 201
        assert 'radar' in data['message']['body']
        assert data['message']['id'] is not None # make sure id is populated
        assert data['message']['isPalindrome'] is True

    def test_delete_message(self):
        response = self.app.delete("/api/v1/messages/3")
        assert response.status_code == 200
        assert 'message deleted' in str(response.data)


if __name__ == '__main__':
    unittest.main()
